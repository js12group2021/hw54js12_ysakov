import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-cell',
  templateUrl: './cell.component.html',
  styleUrls: ['./cell.component.css']
})
export class CellComponent {
  @Input() btnStyle = 'btn-default';

  myClickFunction(event:any) {
    this.btnStyle = 'btn-change';

  }

}
